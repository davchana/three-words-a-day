Use emotions, feelings, reactions, actions, best/worst parts. No Diary, No Sentences, just three words or less 

Example:

* Feeling mostly: `________`
* Concerned about: `_________`
* Day was: `________`